<?php 

/**
* Management Data
* @since 1.0.0
* @author Dodi Alfredo Situmeang
*/

if( !class_exists('Management_Data') ){

	class Management_Data{
		
		protected static $instance = null;

		public static function instance(){
			if(null == self::$instance){
				self::$instance = new self(); 
			}
			return self::$instance;
		}

		function __construct(){
			//add admin menu
			add_action('admin_menu', array($this, 'admin_menu'));

			//add shortcode
	        add_shortcode( 'add-list-shortcode', array($this, 'add_list_shortcode' ));
		}

		function admin_menu(){

			//main menu
			add_menu_page(
				'Plugin Assesment', //page title
				'Plugin Assesment', //menu title
				'manage_options', //capability
				'assesment_list_data', //parent menu slug
				array($this,'show_data_page') , //function show data
				'dashicons-tickets' 
			);

			//submenu
	        add_submenu_page(
		        'assesment_list_data', //parent slug
		        'Add New Data', //sub menu page title
		        'Add New', //sub menu title
		        'manage_options', //capability
		        'assesment_list_data_add', //sub menu slug
	       		 array($this,'add_data_page') //function add data
	       	); 

		}

		//function to show data on menu
		function show_data_page(){

			global $wpdb;

			if(is_multisite()){
				$this->plugin_table = $wpdb->prefix.$wpdb->blogid.'_form_data';
			}else{
				$this->plugin_table = $wpdb->prefix.'form_data';
			}

			$data = $wpdb->get_results( "SELECT * FROM `$this->plugin_table`", ARRAY_A);

			if( !empty($data) ){
				echo '
					<table style="width:100%">
						<tr>
							<th>Name</th>
							<th>Email</th> 
							<th>Messages</th>
						</tr>';

				
					foreach ($data as $value){
						echo '<tr>';
						echo '<td>'. $value['name']. '</td>';
						echo '<td>'. $value['email']. '</td>';
						echo '<td>'. $value['message']. '</td>';
						echo '<tr>';
					}

					
				echo '</table>';
			}else{
				echo "<p>No Data</p>";
			}

		}


		//function to add data and call input form
		function add_data_page(){

			if( isset($_POST['submit']) && $_POST['submit'] ){
				$this->add_data($_POST['name'], $_POST['email'], $_POST['message']);
			}

			require_once 'form-input.php';
			
		}

		//function to call validate function and send email
		function add_data($name, $email, $message){
			global $wpdb;

			$max_id = '';
			$return = '';

			$name = $_POST['name'];
			$email = $_POST['email'];
			$message = $_POST['message'];

			if(is_multisite()){
				$this->plugin_table = $wpdb->prefix.$wpdb->blogid.'_form_data';
			}else{
				$this->plugin_table = $wpdb->prefix.'form_data';
			}

			//get max id on table
			$get_max_id = $wpdb->get_row(" SELECT max(form_id) FROM $this->plugin_table", ARRAY_A);

			foreach ($get_max_id as $id_max) {
				$max_id = $id_max;
			}
			
			//if there is no id, then return 1 else max id on table added 1
			if(empty($max_id)){
				$max_id = 1;
			}else{
				$max_id = $max_id+1;
			}
			
			//validation
			$validate = $this->validation($name, $email, $message);
			if( $validate == "Success" ){
				$wpdb->query(
					$wpdb->prepare("
						INSERT INTO $this->plugin_table 
						VALUES (%d, %s, %s, %s, NOW(), NOW())", $max_id, $name, $email, $message
					)
				);

				$to = get_option('admin_email');
				$subject = 'Input Data From Userr';
				$body = 'Name: '. $name . '<br>' . 'Email' . $email . '<br>' . 'Message' . $message ;
				$headers = array( 'Content-Type: text/html; charset=UTF-8', 'From: '. bloginfo("name") );

				//send email
				if( wp_mail( $to, $subject, $body, $headers ) ){
					echo "<p style='color:red;'>Email has been sent</p>";
					echo "<p style='color:red;'>Data has been added</p>";
				}else{
					echo "Failed to send email";
				}


			}else{
				echo "<p style='color:red;'>". $validate ."</p>";
				echo "<p style='color:red;'>Validation Failed</p>";
			}

		}

		//function to validate user input
		function validation($name, $email, $message){
			$name = $_POST['name'];
			$email = $_POST['email'];
			$message = $_POST['message'];

			if( empty($name) ){
				$return =  "Please Fill Your Name";
			}elseif( empty($email) ){
				$return = "Please Fill Your Email";
			}elseif( empty($message) ){
				$return = "Please Fill Your Message";
			}else{
				$return = "Success";
			}

			return $return;

		}

		//function to show data on front side
		function add_list_shortcode() {
	        ob_start();
	        $this->show_data_page();
	        return ob_get_clean();
	    }

	}
	
}

/*
if( !function_exists('management_data') ){

	function management_data(){
		return Management_Data::instance();
	}

}

management_data();
*/