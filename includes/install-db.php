<?php 

/**
* Create table form_data
* @since 1.0.0
* @author Dodi Alfredo Situmeang
*/
if( !class_exists('Install_DB') ){
	
	class Install_DB{

		protected static $instance = null;

		public static function instance(){
			if(null == self::$instance){
				self::$instance = new self(); 
			}
			return self::$instance;
		}

		function __construct(){

			//call table install when plugins is activated
			register_activation_hook( ASSESMENT_PLUGINS_FILE, array($this, 'table_install'));
			
		}

		//function to create table form_data on database
		function table_install(){
			global $wpdb;

			if(is_multisite()){
				$this->plugin_table = $wpdb->prefix.$wpdb->blogid.'_form_data';
			}else{
				$this->plugin_table = $wpdb->prefix.'form_data';
			}

			$charset = $wpdb->get_charset_collate();
			$sql_add_table = "CREATE TABLE IF NOT EXISTS $this->plugin_table (
					`form_id` int(20) NOT NULL auto_increment,
					`name` varchar(10) NOT NULL,
					`email` varchar(50) NOT NULL,
					`message` varchar(50) NOT NULL,
					`created_at` datetime NOT NULL,
					`updated_at` datetime NOT NULL,
					PRIMARY KEY  (`form_id`)
				) $charset " ;

			require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	        dbDelta($sql_add_table);

		}
	}

}

/*
if( !function_exists('install_db') ){

	function install_db(){
		return Install_DB::instance();
	}

}

install_db();
*/