<?php 

/**
 * Plugin Name: Plugin Assesment
 * Plugin URI: 
 * Description: Assesment Input and Send Email
 * Version: 2.0.0
 * Author: Dodi Alfredo Situmeang
 * Author URI: 
 */

//define location file
defined('ASSESMENT_PLUGINS_FILE') || define('ASSESMENT_PLUGINS_FILE', __FILE__);

if( !class_exists('Assesment') ){

	/**
	 * Create Plugin Assesment
	 * @since 1.0.0
	 */
	class Assesment {
			
		protected static $instance = null;

		public static function instance(){
			if(null == self::$instance){
				self::$instance = new self(); 
			}
			return self::$instance;
		}

		//initiate Class Install DB and Management Data
		function __construct(){
			$this->init();

			Install_DB::instance();
			Management_Data::instance();
		}

		function init(){
			require_once 'includes/install-db.php';
			require_once 'includes/management-data.php';
		}

	}

}

if( !function_exists('assesment') ){

	function assesment(){
		return Assesment::instance();
	}

}

assesment();